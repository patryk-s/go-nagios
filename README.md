**Note:** This project was originally forked from https://github.com/newrelic/go_nagios

Go Nagios
=========

This is a package with simple support for writing Nagios checks in Go.
Includes a few simple types and methods for behaving as a Nagios check.

Supports aggregating multiple check statuses into a final status and
message, with the highest priority winning out.

Contributions
-------------

Contributions are more than welcome. Bug reports with specific reproduction
steps are great. If you have a code contribution you'd like to make, open a
pull request with suggested code.

Pull requests should:

 * Clearly state their intent in the title
 * Have a description that explains the need for the changes
 * Include tests!
 * Not break the public API
