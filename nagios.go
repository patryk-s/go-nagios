package nagios

import (
	"fmt"
	"os"
	"strings"
)

// ExitCode holds the exit code expected by Nagios
type ExitCode int

// The values with which a Nagios check can exit
const (
	Ok ExitCode = iota
	Warning
	Critical
	Unknown
)

// Maps the StatusVal entries to output strings required by nagios
var (
	valMessages = []string{
		"OK:",
		"WARNING:",
		"CRITICAL:",
		"UNKNOWN:",
	}
)

// A Status type representing a Nagios check status. The Value is the exit code
// expected for the check and the Message is the specific output string.
type Status struct {
	Message string
	Value   ExitCode
}

// Aggregate takes a bunch of Status pointers and finds the highest value, then
// combines all the messages. Things win in the order of highest to lowest.
// Any additionaly provided strings will be used as the prefix for the final message.
func Aggregate(otherStatuses []*Status, msg ...string) *Status {
	msgPrefix := strings.Join(msg, " ")
	baseStatus := &Status{
		Message: msgPrefix,
		Value:   Ok}
	if len(otherStatuses) == 0 {
		return baseStatus
	}
	baseStatus.Message += " ["
	for i, s := range otherStatuses {
		if baseStatus.Value < s.Value {
			baseStatus.Value = s.Value
		}
		if i == 0 {
			baseStatus.Message += "'" + s.Message + "'"
		} else {
			baseStatus.Message += ", '" + s.Message + "'"
		}
	}
	baseStatus.Message += "]"
	return baseStatus
}

// ExitWithAggregate exits with the highest status found in the passed slice,
// combining all of the individual messages.
// Any additionaly provided strings will be used as the prefix for the final message.
func ExitWithAggregate(otherStatuses []*Status, msg ...string) {
	ExitWithStatus(Aggregate(otherStatuses, msg...))
}

// ExitUnknown exits with an UNKNOWN status and appropriate message
func ExitUnknown(format string, a ...interface{}) {
	ExitWithStatus(&Status{fmt.Sprintf(format, a...), Unknown})
}

// ExitCritical exits with a CRITICAL status and appropriate message
func ExitCritical(format string, a ...interface{}) {
	ExitWithStatus(&Status{fmt.Sprintf(format, a...), Critical})
}

// ExitWarning exits with a WARNING status and appropriate message
func ExitWarning(format string, a ...interface{}) {
	ExitWithStatus(&Status{fmt.Sprintf(format, a...), Warning})
}

// ExitOk exits with an OK status and appropriate message
func ExitOk(format string, a ...interface{}) {
	ExitWithStatus(&Status{fmt.Sprintf(format, a...), Ok})
}

// ExitWithStatus exits with a particular Status
func ExitWithStatus(status *Status) {
	fmt.Fprintln(os.Stdout, valMessages[status.Value], status.Message)
	os.Exit(int(status.Value))
}
