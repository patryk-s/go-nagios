## Changelog

### Unreleased

### v0.4.1
* Don't print `[]` for empty Aggregate slice

### v0.4.0
Breaking changes:
* `Aggregate()` is now a function returning a `*Status`

Other changes:
* New `ExitWithAggregate()` function
* `Aggregate()` accepts an optional `[]string` which will form the prefix of the final message
* Add tests
### v0.3.0
* `Exit*` functions accept format strings

### v0.2.0
Breaking changes:
* drop unneeded *nagios* prefixes (`NAGIOS_OK` -> `Ok`, `NagiosStatus` -> `Status`)
* rename functions (`Ok` -> `ExitOk`)
* `ExitCritical()` now accepts *string* instead of *err*
