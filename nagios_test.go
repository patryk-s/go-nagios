package nagios

import (
	"fmt"
	"os"
	"os/exec"
	"testing"
)

var (
	pods = []struct {
		Name    string
		Message string
		Value   ExitCode
	}{
		{
			Name:    "namespace-one/pod-one",
			Message: "Message one",
			Value:   Warning,
		},
		{
			Name:    "namespace-two/pod-two",
			Message: "Message two",
			Value:   Critical,
		},
	}
	want = " ['namespace-one/pod-one, Reason: Message one', 'namespace-two/pod-two, Reason: Message two']"
)

func TestAggregateDefault(t *testing.T) {
	statuses := make([]*Status, 0)
	for _, pod := range pods {
		statuses = append(statuses, &Status{
			Message: fmt.Sprintf("%s, Reason: %s", pod.Name, pod.Message),
			Value:   pod.Value})
	}
	testStatus := Aggregate(statuses)
	if testStatus.Value != Critical {
		t.Errorf("Value: got %d, want %d", testStatus.Value, Critical)
	}
	if testStatus.Message != want {
		t.Errorf("Message: got %s, want %s", testStatus.Message, want)
	}
}

func TestExitWithAggregateOk(t *testing.T) {
	statuses := make([]*Status, 0)
	want := "OK: 4" + "\n"
	if os.Getenv("TEST_EXIT") == "1" {
		ExitWithAggregate(statuses, fmt.Sprintf("%d", 4))
		return
	}
	cmd := exec.Command(os.Args[0], "-test.run=TestExitWithAggregateOk")
	cmd.Env = append(os.Environ(), "TEST_EXIT=1")
	out, err := cmd.Output()
	if err == nil {
		if string(out) != want {
			t.Errorf("Message: got \"%s\", want \"%s\"", string(out), want)
		}
		return
	}
	t.Errorf("process ran with err %v, want exit status 0", err)
}

func TestExitWithAggregateCritical(t *testing.T) {
	statuses := make([]*Status, 0)
	for _, pod := range pods {
		statuses = append(statuses, &Status{
			Message: fmt.Sprintf("%s, Reason: %s", pod.Name, pod.Message),
			Value:   pod.Value})
	}
	want := "CRITICAL: " + want + "\n"
	if os.Getenv("TEST_EXIT") == "1" {
		ExitWithAggregate(statuses)
		return
	}
	cmd := exec.Command(os.Args[0], "-test.run=TestExitWithAggregateCritical")
	cmd.Env = append(os.Environ(), "TEST_EXIT=1")
	out, err := cmd.Output()
	if e, ok := err.(*exec.ExitError); ok && e.ExitCode() == 2 {
		if string(out) != want {
			t.Errorf("Message: got \"%s\", want \"%s\"", string(out), want)
		}
		return
	}
	t.Errorf("process ran with err %v, want exit status 2", err)
}

func TestExitOk(t *testing.T) {
	if os.Getenv("TEST_EXIT") == "1" {
		ExitOk("This is fine.")
		return
	}
	cmd := exec.Command(os.Args[0], "-test.run=TestExitOk")
	cmd.Env = append(os.Environ(), "TEST_EXIT=1")
	err := cmd.Run()
	if err == nil {
		return
	}
	t.Errorf("process ran with err %v, want exit status 0", err)
}

func TestExitWarning(t *testing.T) {
	if os.Getenv("TEST_EXIT") == "1" {
		ExitWarning("This is a warning.")
		return
	}
	cmd := exec.Command(os.Args[0], "-test.run=TestExitWarning")
	cmd.Env = append(os.Environ(), "TEST_EXIT=1")
	err := cmd.Run()
	if e, ok := err.(*exec.ExitError); ok && e.ExitCode() == 1 {
		return
	}
	t.Errorf("process ran with err %v, want exit status 1", err)
}

func TestExitCritical(t *testing.T) {
	if os.Getenv("TEST_EXIT") == "1" {
		ExitCritical("This is critical.")
		return
	}
	cmd := exec.Command(os.Args[0], "-test.run=TestExitCritical")
	cmd.Env = append(os.Environ(), "TEST_EXIT=1")
	err := cmd.Run()
	if e, ok := err.(*exec.ExitError); ok && e.ExitCode() == 2 {
		return
	}
	t.Errorf("process ran with err %v, want exit status 2", err)
}
